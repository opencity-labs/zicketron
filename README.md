## Zicketron (in attesa del Ticketron 1.0)

### Varibili d'ambiente
| VAR                           | DESCR                                                                                                                 |
|-------------------------------|-----------------------------------------------------------------------------------------------------------------------|
| KAFKA_TOPIC                   | Topic degli update che arrivano dal CzRM                                                                              |
| KAFKA_SERVER                  | Endpoint kafka                                                                                                        |
| KAFKA_GROUP_ID                | Consumer group                                                                                                        |
| COMMAND                       | Comando Krun (`/var/app/run.php`)                                                                                     |
| EVENT_ENTITIES                | Tipologia di `entity` da processare <br/>(i valori possibili sono `case`, `account`, `contentversion`, `commento__c`) |
| CZRM_BASENAME                 | Endpoint CzRM                                                                                                         |
| CZRM_AUTH_TYPE                | Tipologia di autenticazione <br/>(il valore va scelto tra `WSO2` e `SF`)                                              |
| CZRM_AUTH_TOKEN_ENDPOINT      | Endpoint per effettuare l'autenticazione: risponde con un access token                                                |
| CZRM_AUTH_TOKEN_USER          | Solo per autenticazione type `SF`                                                                                     |
| CZRM_AUTH_TOKEN_PASSWORD      | Solo per autenticazione type `SF`                                                                                     |
| CZRM_AUTH_TOKEN_GRANT_TYPE    | Solo per autenticazione type `SF` (`password`)                                                                        |
| CZRM_AUTH_TOKEN_CLIENT_ID     |                                                                                                                       |
| CZRM_AUTH_TOKEN_CLIENT_SECRET |                                                                                                                       |
| SDC_BASENAME                  | Endpoint SegnalaCI.v4                                                                                                 |
| SDC_AUTH_TOKEN_USER           | Username operatore API                                                                                                |
| SDC_AUTH_TOKEN_PASSWORD       | Password operatore API                                                                                                |
| SDC_AUTH_TOKEN_ADMIN_USER     | Username admin                                                                                                        |
| SDC_AUTH_TOKEN_ADMIN_PASSWORD | Password admin (per generare gli uffici)                                                                              |
| SDC_SERVICE_ID                | Id del servizio con cui vengono create le segnalazioni (`inefficiencies`)                                             |
| SDC_OPERATOR_ID               | Id dell'operatore SDC_AUTH_TOKEN_USER <br/>**Questa variabile sarà rimossa e l'id sarà ottenuto dal token**           |


### Codici di errore
| COD | DESC                                   |
|-----|----------------------------------------|
| 2   | CASE_NOT_FOUND                         |
| 3   | ACCOUNT_NOT_FOUND                      |
| 4   | FAIL_PATCH_CASE                        |
| 5   | FAIL_PATCH_ACCOUNT                     |
| 6   | CONTENT_VERSION_NOT_FOUND              |
| 7   | CASE_FROM_CONTENT_VERSION_NOT_FOUND    |
| 8   | CONTENT_VERSION_DATA_NOT_FOUND         |
| 9   | CASE_WITHOUT_ACCOUNT_ID                |
| 10  | COMMENT_NOT_FOUND                      |
| 11  | CASE_FROM_COMMENT_NOT_FOUND            |
| 12  | MESSAGE_NOT_FOUND                      |
| 100 | FAIL_GET_USER_GROUPS                   |
| 101 | FAIL_CREATE_USER_GROUP                 |
| 102 | FAIL_APPLICATION_CREATE                |
| 103 | APPLICATION_FROM_EXTERNAL_ID_NOT_FOUND |
| 104 | FAIL_BINARY_CREATE                     |
| 105 | FAIL_USER_CREATE                       |
| 106 | FAIL_PUT_BINARY_TO_APPLICATION         |
| 107 | FAIL_PATCH_APPLICATION_BINARY          |
| 108 | FAIL_ACCEPT_APPLICATION                |
| 109 | FAIL_ASSIGN_APPLICATION                |
| 110 | FAIL_GET_APPLICATION_HISTORY           |
| 111 | FAIL_CREATE_MESSAGE                    |
| 112 | USER_FROM_EXTERNAL_ID_NOT_FOUND        |


### Utilizzo
Il comando run.php legge lo standard input, esempio:

```
cat init.d/Case.json | php run.php
```

Si propone di risolvere:
 - https://gitlab.com/opencity-labs/ticketron/-/issues/8
 - https://gitlab.com/opencity-labs/ticketron/-/issues/15
 - https://gitlab.com/opencity-labs/ticketron/-/issues/28
 - https://gitlab.com/opencity-labs/ticketron/-/issues/29
