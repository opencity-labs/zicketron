FROM php:cli

RUN apt-get update && apt-get install -y --no-install-recommends \
    nano \
    procps \
    unzip \
    git \
    bash \
  && rm -rf /var/lib/apt/lists/*

# Copy application codebase
WORKDIR /var/app
COPY ./ /var/app

ENV COMPOSER_ALLOW_SUPERUSER=1
RUN curl --silent --show-error https://getcomposer.org/installer | php && \
    php composer.phar install --prefer-dist --no-progress --optimize-autoloader --classmap-authoritative --no-interaction && \
    php composer.phar clear-cache

# Add Krun registry.gitlab.com/opencity-labs/krun/main:latest
#COPY --from=registry.gitlab.com/opencity-labs/krun:0.4.0 /krun /bin/krun

COPY --from=registry.gitlab.com/opencity-labs/krun:latest-arm /krun /bin/krun_arm
COPY --from=registry.gitlab.com/opencity-labs/krun:latest-x86 /krun /bin/krun_x86

ARG TARGETPLATFORM
RUN if [ "$TARGETPLATFORM" = "linux/arm64" ]; then ln -s /bin/krun_arm /bin/krun; else ln -s /bin/krun_x86 /bin/krun; fi 

ENTRYPOINT [ "/bin/krun" ]
