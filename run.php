#!/usr/local/bin/php
<?php

require __DIR__ . '/vendor/autoload.php';

if (file_exists(__DIR__ . '/dev-env.php')) {
    require __DIR__ . '/dev-env.php';
}

//use Opencontent\CheckDryRunCommand;
use Opencontent\DebugCommand;
use Opencontent\RepushAttachmentCommand;
use Opencontent\RunCommand;
use Symfony\Component\Console\Application;

$application = new Application('Zicketron', '0.1');
$command = new RunCommand();
$application->add($command);
$application->setDefaultCommand($command->getName());

$debugCommand = new DebugCommand();
$application->add($debugCommand);
$application->add(new RepushAttachmentCommand());
//$application->add(new CheckDryRunCommand());

$application->run();
