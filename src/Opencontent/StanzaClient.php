<?php

namespace Opencontent;

use GuzzleHttp\Client;
use GuzzleHttp\Exception\ClientException;
use Opencontent\Exceptions\ApplicationByExternalIdNotFound;
use Opencontent\Exceptions\CommandException;
use Opencontent\Exceptions\FailCreateUserGroup;
use Opencontent\Exceptions\FailAcceptApplication;
use Opencontent\Exceptions\FailApplicationCreate;
use Opencontent\Exceptions\FailAssignApplication;
use Opencontent\Exceptions\FailBinaryCreate;
use Opencontent\Exceptions\FailCreateMessage;
use Opencontent\Exceptions\FailGetUserGroups;
use Opencontent\Exceptions\FailPatchApplicationBinaryWithExternalId;
use Opencontent\Exceptions\FailPutBinaryToApplication;
use Opencontent\Exceptions\FailUserCreate;
use Opencontent\Exceptions\MessageNotFound;
use Opencontent\Exceptions\UserByIdNotFound;
use Opencontent\Exceptions\FailApplicationMessages;
use Psr\Http\Client\ClientExceptionInterface;
use Psr\Http\Message\ResponseInterface;
use Psr\Log\LoggerInterface;
use RuntimeException;

class StanzaClient
{
    protected Client $client;

    protected static ?string $token = null;

    protected static ?string $adminToken = null;

    protected string $locale;

    protected string $apiUri;

    protected string|false $password;

    protected string|false $username;

    protected string|false $adminPassword;

    protected string|false $adminUsername;

    protected string|false $serviceId;

    /**
     * @var string[][]
     */
    protected array $headers;

    /**
     * @param LoggerInterface $logger
     * @throws ClientExceptionInterface
     */
    public function __construct(protected readonly LoggerInterface $logger)
    {
        $this->username = getenv('SDC_AUTH_TOKEN_USER');
        $this->password = getenv('SDC_AUTH_TOKEN_PASSWORD');
        $this->adminUsername = getenv('SDC_AUTH_TOKEN_ADMIN_USER');
        $this->adminPassword = getenv('SDC_AUTH_TOKEN_ADMIN_PASSWORD');
        $this->apiUri = rtrim(getenv('SDC_BASENAME'), '/');
        $this->locale = 'it';
        $this->client = new Client();
        $this->serviceId = getenv('SDC_SERVICE_ID');
        $this->headers = [
            'Content-Type' => 'application/json',
            'Authorization' => 'Bearer ' . $this->getAccessToken(),
        ];
    }

    /**
     * @return string|null
     * @throws ClientExceptionInterface
     */
    public function getAccessToken(): ?string
    {
        if (self::$token === null) {
            $response = json_decode(
                (string)$this->client->request(
                    'POST',
                    $this->apiUri . '/auth',
                    [
                        'json' => [
                            'username' => $this->username,
                            'password' => $this->password,
                        ],
                    ]
                )->getBody(),
                true
            );
            self::$token = $response['token'];
        }

        return self::$token;
    }

    /**
     * @return string|null
     * @throws ClientExceptionInterface
     */
    protected function getAdminAccessToken(): ?string
    {
        if (self::$adminToken === null) {
            $response = json_decode(
                (string)$this->client->request(
                    'POST',
                    $this->apiUri . '/auth',
                    [
                        'json' => [
                            'username' => $this->adminUsername,
                            'password' => $this->adminPassword,
                        ],
                    ]
                )->getBody(),
                true
            );
            self::$adminToken = $response['token'];
        }

        return self::$adminToken;
    }

    /**
     * @param string $id
     * @param int $version
     * @return array
     * @throws ApplicationByExternalIdNotFound
     * @throws ClientExceptionInterface
     */
    public function getApplicationByExternalId(string $id, int $version = 1): array
    {
        try {
            $this->logger->debug(' - Get stanza application by external id ' . $id);
            $response = (string)$this->client->request(
                'GET',
                $this->apiUri . '/applications/byexternal-id/' . $id,
                [
                    'headers' => $this->headers,
                    'version' => $version,
                ]
            )->getBody();
            $result = json_decode($response, true);

            $this->logger->warning(' - Stanza application id is ' . $result['id']);
            return $result;
        } catch (ClientException $e) {
            if ($e->getResponse()->getStatusCode() === 404){
                CommandException::throwApplicationByExternalIdNotFound($e);
            }
            throw $e;
        }
    }

    /**
     * @param string $id
     * @param int $version
     * @return array
     * @throws ClientExceptionInterface
     */
    public function getApplicationByUuid(string $id, int $version = 1): array
    {
        $this->logger->debug(' - Get stanza application by uuid ' . $id);
        $response = (string)$this->client->request(
            'GET',
            $this->apiUri . '/applications/' . $id,
            [
                'headers' => $this->headers,
                'version' => $version,
            ]
        )->getBody();
        return json_decode($response, true);
    }

    /**
     * @param string $method
     * @param $uri
     * @param array $options
     * @return ResponseInterface
     * @throws ClientExceptionInterface
     */
    public function request(string $method, $uri = '', array $options = []): ResponseInterface
    {
        $this->logger->debug(" - Get $method $uri");
        $options['headers'] = $this->headers;
        return $this->client->request($method, $uri, $options);
    }

    /**
     * @param string $id
     * @return array
     * @throws ClientExceptionInterface
     */
    public function getAttachmentByApplicationByUuid(string $applicationId, string $id): array
    {
        $this->logger->debug(' - Get stanza application by uuid ' . $applicationId);
        $response = (string)$this->client->request(
            'GET',
            $this->apiUri . '/applications/' . $applicationId . '/attachments/' . $id,
            [
                'headers' => $this->headers,
            ]
        )->getBody();
        $result = json_decode($response, true);

        return $result;
    }

    /**
     * @param string $id
     * @return array
     * @throws FailApplicationMessages
     */
    public function getApplicationMessages(string $id): array
    {
        try {
            $this->logger->debug(' - Get stanza application messages by id ' . $id);
            $response = (string)$this->client->request(
                'GET',
                $this->apiUri . '/applications/' . $id . '/messages',
                [
                    'headers' => $this->headers,
                ]
            )->getBody();

            return json_decode($response, true);
        } catch (ClientExceptionInterface $e) {
            CommandException::throwFailApplicationMessages($e);
        }
    }

    /**
     * @param string $id
     * @return array
     * @throws UserByIdNotFound
     */
    public function getUserById(string $id): array
    {
        try {
            $this->logger->debug(' - Get stanza user by id ' . $id);
            $response = (string)$this->client->request(
                'GET',
                $this->apiUri . '/users/' . $id,
                [
                    'headers' => $this->headers,
                ]
            )->getBody();
            $result = json_decode($response, true);

            $this->logger->warning(' - Stanza user id is ' . $result['id']);
            return $result;
        } catch (ClientExceptionInterface $e) {
            CommandException::throwUserByIdNotFound($e);
        }
    }

//    /**
//     * @param $id
//     * @return array
//     * @throws ClientExceptionInterface
//     */
//    public function getApplicationById($id): array
//    {
//        $this->logger->debug(' - Get stanza application by id ' . $id);
//        $response = (string)$this->client->request(
//            'GET',
//            $this->apiUri . '/applications/' . $id,
//            [
//                'headers' => $this->headers,
//            ]
//        )->getBody();
//
//        return json_decode($response, true);
//    }

    /**
     * @param array $account
     * @return array
     * @throws FailUserCreate
     */
    public function createUserFromAccount(array $account): array
    {
        try {
            $fiscalCode = Utils::getFirstNotEmpty($account, 'Codice_Fiscale__c', 'Codice_Fiscale__pc');
            if ($user = $this->getUserByFiscalCode($fiscalCode)) {
                return $user;
            }

            $data = [
                'nome' => $account['FirstName'] ?? '',
                'cognome' => $account['LastName'] ?? '',
                'email' => strtolower(Utils::getFirstNotEmpty($account, 'Email__c', 'PersonEmail')),
                'codice_fiscale' => strtoupper($fiscalCode),
                'cellulare' => $account['Phone'] ?? '',
                'telefono' => $account['Phone'] ?? '',
                'indirizzo_domicilio' => $account['ShippingStreet'] ?? '',
                'cap_domicilio' => $account['ShippingPostalCode'] ?? '',
                'citta_domicilio' => $account['ShippingCity'] ?? '',
                'provincia_domicilio' => $account['ShippingState'] ?? '',
                'stato_domicilio' => $account['ShippingCountry'] ?? '',
            ];

            if (!empty($account['PersonBirthdate'])) {
                $data['data_nascita'] = date('c', strtotime($account['PersonBirthdate']));
            }

            $this->logger->debug(' - Create stanza user from account ' . $account['Id']);
            $response = (string)$this->client->request(
                'POST',
                $this->apiUri . '/users',
                [
                    'headers' => [
                        'Authorization' => 'Bearer ' . $this->getAccessToken(),
                    ],
                    'json' => $data,
                ]
            )->getBody();

            return json_decode($response, true);
        } catch (ClientExceptionInterface $e) {
            CommandException::throwFailUserCreate($e);
        }
    }

    /**
     * @param array $case
     * @param ?array $user
     * @return array
     * @throws FailApplicationCreate
     */
    public function createApplicationFromCase(array $case, ?array $user = null): array
    {
        $status = 2000; // $case['Stato_Front_End__c'] == 'In carico' ? 2000 : 1900; //@todo

        $data = [
            'external_id' => $case['Id'],
            'service' => $this->serviceId,
            'status' => $status,
            'created_at' => $case['CreatedDate'],
            'data' => [
                'applicant' => [
                    'data' => [
                        'email_address' => $user['email'] ?? Utils::getFirstNotEmpty($case, 'Email__c', 'ContactEmail', 'PersonEmail'),
                        'phone_number' => $user['cellulare'] ?? Utils::getFirstNotEmpty($case, 'Phone', 'PersonMobilePhone'),
                        'completename' => [
                            'data' => [
                                'name' => $user['nome'] ?? $case['FirstName'] ?? '',
                                'surname' => $user['cognome'] ?? $case['LastName'] ?? '',
                            ],
                        ],
                        'fiscal_code' => [
                            'data' => [
                                'fiscal_code' => $user['codice_fiscale'] ?? Utils::getFirstNotEmpty(
                                        $case,
                                        'Codice_Fiscale__c',
                                        'Codice_Fiscale__pc',
                                        'AccountId',
                                        'Id'
                                    ),
                            ],
                        ],
                        'person_identifier' => $user['codice_fiscale'] ?? Utils::getFirstNotEmpty(
                            $case,
                            'Codice_Fiscale__c',
                            'Codice_Fiscale__pc',
                            'AccountId',
                            'Id'
                        ),
                    ],
                ],
                'type' => Vocabularies::mapType($case['Categoria_del_Cittadino_Backend__c']),
                'micromacrocategory' => [
                    'label' => $case['MacrocategoriaSegnalaCi__c'] . ': ' . $case['Categoria_Backend__c'],
                    'value' => $case['Categoria__c'],
                ],
                'details' => $case['Description'],
                'subject' => $case['Subject'],
            ],
        ];

        if (isset($user['id'])){
            $data['user'] = $user['id'];
        }

        if (!empty($case['Indirizzo__Latitude__s']) && !empty($case['Indirizzo__Longitude__s'])) {
            $data['data']['address'] = [
                'lat' => $case['Indirizzo__Latitude__s'],
                'lon' => $case['Indirizzo__Longitude__s'],
                'address' => [
                    'country' => $case['Indirizzo__c']['country'] ?? '',
                    'country_code' => $case['Indirizzo__CountryCode__s'],
                    'county' => $case['Indirizzo__c']['state'] ?? '',
                    'postcode' => $case['Indirizzo__PostalCode__s'],
                    'road' => $case['Indirizzo__Street__s'],
                    'town' => $case['Indirizzo__City__s'],
                ],
            ];
        }elseif (!empty($case['Indirizzo__Street__s'])){
            $nominatimQuery = $case['Indirizzo__Street__s'];
            $nominatimQuery .= !empty($case['Indirizzo__City__s']) ? ' ' . $case['Indirizzo__City__s'] : ' Genova';
            $nominatim = Utils::getNominatimLocation($nominatimQuery);
            if (!empty($nominatim)){
                $data['data']['address'] = $nominatim;
            }
        }

        $this->logger->debug(
            ' - Create stanza application from case ' . $case['Id'] . ' with service id ' . $this->serviceId
        );
        try {
            $response = (string)$this->client->request(
                'POST',
                $this->apiUri . '/applications',
                [
                    'headers' => $this->headers,
                    'json' => $data,
                ]
            )->getBody();

            return json_decode($response, true);
        } catch (ClientExceptionInterface $e) {
            CommandException::throwFailApplicationCreate($e);
        }
    }

    /**
     * @param string $fiscalCode
     * @return array|null
     * @throws ClientExceptionInterface
     */
    protected function getUserByFiscalCode(string $fiscalCode): ?array
    {
        $this->logger->debug(' - Get stanza user by cf ' . $fiscalCode);
        $response = (string)$this->client->request(
            'GET',
            $this->apiUri . '/users',
            [
                'headers' => $this->headers,
                'query' => ['cf' => $fiscalCode],
            ]
        )->getBody();
        $response = json_decode($response, true);
        if (count($response) > 0) {
            return $response[0];
        }

        return null;
    }

    /**
     * @param array $data
     * @return array
     * @throws FailBinaryCreate
     */
    public function uploadBinary(array $data): array
    {
        try {
            if (!file_exists($data['path'])) {
                throw new RuntimeException('File ' . $data['path'] . ' not found');
            }
            $this->logger->debug(" - Get upload pre-signed uri for " . $data['original_filename']);
            $baseUri = str_replace('/api', '', $this->apiUri);

            $fileContents = file_get_contents($data['path']);
            if (mb_strlen($fileContents) === 0) {
                $this->logger->error("Error: invalid file contents for file " . $data['path']);
                $fileContents = '[File not found]';
            }
            $size = mb_strlen($fileContents);
            $mimeType = $data['mime_type'];

            $response = (string)$this->client->request(
                'POST',
                $baseUri . '/' . $this->locale . '/upload',
                [
                    'headers' => [
                        'Authorization' => 'Bearer ' . $this->getAccessToken(),
                    ],
                    'json' => [
                        'name' => $data['original_filename'],
                        'original_filename' => $data['original_filename'],
                        'size' => $size,
                        'protocol_required' => false,
                        'mime_type' => $mimeType,
                    ],
                ]
            )->getBody();
            $fileInfo = json_decode($response, true);
            $this->logger->debug(" - Put file to " . substr($fileInfo['uri'], 0, 100) . '...');
            $curl = curl_init();
            $curlOptions = [
                CURLOPT_URL => $fileInfo['uri'],
                CURLOPT_RETURNTRANSFER => 1,
                CURLOPT_CUSTOMREQUEST => "PUT",
                CURLOPT_ENCODING => "",
                CURLOPT_POST => 1,
                CURLOPT_MAXREDIRS => 10,
                CURLOPT_TIMEOUT => 300,
                CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
                CURLOPT_INFILESIZE => $size,
                CURLOPT_HTTPHEADER => [
                    "Accept: */*",
                    "Accept-Encoding: gzip, deflate",
                    "Cache-Control: no-cache",
                    "Connection: keep-alive",
                    "Content-Length: " . $size,
                    "Content-Type: multipart/form-data",
                ],
            ];
            curl_setopt_array($curl, $curlOptions);
            curl_setopt($curl, CURLOPT_POSTFIELDS, $fileContents);
            curl_exec($curl);

            $this->logger->debug(" - Update upload " . $fileInfo['id']);
            $fileHash = hash('sha256', $fileContents);

            $this->client->request(
                'PUT',
                $baseUri . '/' . $this->locale . '/upload/' . $fileInfo['id'],
                [
                    'headers' => [
                        'Authorization' => 'Bearer ' . $this->getAccessToken(),
                    ],
                    'json' => [
                        'file_hash' => $fileHash,
                        'check_signature' => false,
                    ],
                ]
            )->getBody();

            return [
                "name" => $data['original_filename'],
                "url" => null,
                "size" => $size,
                "type" => $data['mime_type'],
                "data" => $fileInfo,
                "originalName" => $data['original_filename'],
                "hash" => $fileHash,
                "preview" => null,
            ];
        } catch (ClientExceptionInterface $e) {
            CommandException::throwFailBinaryCreate($e);
        }
    }

    /**
     * @param array $uploadFile
     * @param array $application
     * @param string $dataKey
     * @param string $caseId
     * @param string $contentVersionId
     * @return string[]
     * @throws FailPatchApplicationBinaryWithExternalId
     * @throws FailPutBinaryToApplication
     */
    public function addBinaryToApplication(
        array $uploadFile,
        array $application,
        string $dataKey,
        string $caseId,
        string $contentVersionId
    ): array {
        $this->logger->debug(" - Update stanza application $dataKey data with binary " . $uploadFile['name']);

        foreach (['images', 'docs'] as $key) {
            $dataByKey = $application['data'][$key] ?? [];
            foreach ($dataByKey as $index => $item) {
                $application['data'][$key][$index]['data']['id'] = $item['id'];
            }
        }
        $currentDataKey = $application['data'][$dataKey] ?? [];

        $requestData = $application;
        if (isset($requestData['payment_data']) && empty($requestData['payment_data'])) {
            unset($requestData['payment_data']);
        }
        if (empty($requestData['external_id'])) {
            $requestData['external_id'] = $caseId;
        }
        $requestData['data'] = array_merge($application['data'], [
            "applicant" => [
                "data" => [
                    "email_address" => $application['data']['applicant.data.email_address'] ?? '',
                    "phone_number" => $application['data']['applicant.data.phone_number'] ?? '',
                    "completename" => [
                        "data" => [
                            "name" => $application['data']['applicant.data.completename.data.name'] ?? '',
                            "surname" => $application['data']['applicant.data.completename.data.surname'] ?? '',
                        ],
                    ],
                    "fiscal_code" => [
                        "data" => [
                            "fiscal_code" => $application['data']['applicant.data.fiscal_code.data.fiscal_code'] ?? '',
                        ],
                    ],
                    "person_identifier" => $application['data']['applicant.data.person_identifier'] ?? '',
                ],
            ],
            $dataKey => array_merge($currentDataKey, [
                [
                    'id' => $uploadFile['data']['id'],
                    'url' => $uploadFile['data']['uri'],
                    'data' => $uploadFile['data'],
                    'name' => $uploadFile['name'],
                    'original_filename' => $uploadFile['name'],
                    'size' => $uploadFile['size'],
                    'protocol_required' => false,
                    'mime_type' => $uploadFile['type'],
                ],
            ]),
        ]);

        try {
            $this->client->request(
                'PUT',
                $this->apiUri . '/applications/' . $application['id'],
                [
                    'headers' => $this->headers,
                    'json' => $requestData,
                ]
            )->getBody();

            return $this->patchApplicationBinaryWithExternalId(
                $application['id'],
                $uploadFile['data']['id'],
                $contentVersionId
            );
        } catch (ClientExceptionInterface $e) {
            CommandException::throwFailPutBinaryToApplication($e);
        }
    }

    /**
     * @param string $applicationId
     * @param string $id
     * @param string $externalId
     * @return string[]
     * @throws FailPatchApplicationBinaryWithExternalId
     */
    public function patchApplicationBinaryWithExternalId(string $applicationId, string $id, string $externalId): array
    {
        $this->logger->debug(" - Patch stanza application $applicationId binary $id with external id $externalId");
        try {
            (string)$this->client->request(
                'PATCH',
                $this->apiUri . '/applications/' . $applicationId . '/attachments/' . $id,
                [
                    'headers' => $this->headers,
                    'json' => [
                        'external_id' => $externalId,
                    ],
                ]
            )->getBody();

            return [
                'id' => $id,
                'external_id' => $externalId,
            ];
        } catch (ClientExceptionInterface $e) {
            CommandException::throwFailPatchApplicationBinaryWithExternalId($e);
        }
    }

    /**
     * @param string $caseOfficeName
     * @return array
     * @throws FailCreateUserGroup
     * @throws FailGetUserGroups
     * @todo serve un api che cerchi per nome dell'ufficio
     */
    public function getUserGroupByName(string $caseOfficeName): array
    {
        $this->logger->debug(" - Find stanza user group by name $caseOfficeName");
        try {
            $userGroups = (array)json_decode(
                (string)$this->client->request(
                    'GET',
                    $this->apiUri . '/user-groups',
                    [
                        'headers' => $this->headers,
                    ]
                )->getBody(),
                true
            );
        } catch (ClientExceptionInterface $e) {
            CommandException::throwFailGetUserGroups($e);
        }

        foreach ($userGroups as $userGroup) {
            if ($userGroup['name'] === trim($caseOfficeName)) {
                return $userGroup;
            }
        }

        return $this->createUserGroupWithName($caseOfficeName);
    }

    /**
     * @param string $caseOfficeName
     * @return array
     * @throws FailCreateUserGroup
     */
    public function createUserGroupWithName(string $caseOfficeName): array
    {
        $this->logger->debug(" - Create stanza user group with name $caseOfficeName");
        try {
            $response = (string)$this->client->request(
                'POST',
                $this->apiUri . '/user-groups',
                [
                    'headers' => [
                        'Authorization' => 'Bearer ' . $this->getAdminAccessToken(),
                    ],
                    'json' => [
                        'name' => trim($caseOfficeName),
                    ],
                ]
            )->getBody();

            return json_decode($response, true);
        } catch (ClientExceptionInterface $e) {
            CommandException::throwCreateUserGroup($e);
        }
    }

//    /**
//     * @param string $caseOfficeName
//     * @return array
//     * @throws ClientExceptionInterface
//     * @throws CommandException
//     * @throws GuzzleException
//     */
//    public function assignCurrentUserToUserGroup(array $userGroup): array
//    {
//        $this->logger->debug(" - Assign current stanza user to group with name " . $userGroup['id']);
//        try {
//            $response = (string)$this->client->request(
//                'POST',
//                $this->apiUri . '/user-groups',
//                [
//                    'headers' => [
//                        'Authorization' => 'Bearer ' . $this->getAdminAccessToken(),
//                    ],
//                    'json' => [
//                        'name' => trim($caseOfficeName),
//                    ],
//                ]
//            )->getBody();
//            *
//            return json_decode($response, true);
//        } catch (ClientException $e) {
//            CommandException::throwCreateUserGroup($e);
//        }
//    }

    /**
     * @param array $application
     * @param string|null $message
     * @return array
     * @throws FailAcceptApplication
     * @throws FailAssignApplication
     */
    public function acceptApplication(array $application, ?string $message = null): array
    {
        $applicationId = $application['id'];

        if ((int)$application['status'] >= 7000){
            $this->logger->debug(" - Stanza application $applicationId already accepted");
            return $application;
        }

        $this->assignApplicationToDefaultUserAndGroup($application);
        $this->logger->debug(" - Accept stanza application $applicationId");
        try {
            $data = $message ? ['message' => $message,] : [];
            $response = (string)$this->client->request(
                'POST',
                $this->apiUri . '/applications/' . $applicationId . '/transition/accept',
                [
                    'headers' => [
                        'Authorization' => 'Bearer ' . $this->getAccessToken(),
                    ],
                    'json' => $data,
                ]
            )->getBody();

            return (array)json_decode($response, true);
        } catch (ClientExceptionInterface $e) {
            CommandException::throwFailAcceptApplication($e);
        }
    }

    /**
     * @param array $application
     * @param array|null $userGroup
     * @return array
     * @throws FailAssignApplication
     */
    public function assignApplicationToUserGroup(array $application, ?array $userGroup = null): array
    {
        $applicationId = $application['id'];
        $userGroupId = $userGroup['id'];
        $userGroupName = $userGroup['name'];

        if (isset($application['user_group_id']) && $application['user_group_id'] === $userGroupId) {
            $this->logger->debug(" - Stanza application $applicationId already assigned to user group");
            return [];
        }
        $data = [
            'user_group_id' => $userGroupId,
            'user_id' => null,
            'message' => '',
            'assigned_at' => date('c'),
        ];

        $this->logger->debug(
            " - Assign stanza application $applicationId to group $userGroupName with payload: " . json_encode($data)
        );
        return $this->doAssign($applicationId, $data);
    }

    /**
     * @param array $application
     * @return array
     * @throws FailAssignApplication
     */
    public function assignApplicationToDefaultUserAndGroup(array $application): array
    {
        $applicationId = $application['id'];
        $operatorId = getenv('SDC_OPERATOR_ID');
        $fallbackUserGroupId = getenv('SDC_FALLBACK_GROUP_ID');

        if (isset($application['operator_id']) && $application['operator_id'] == $operatorId
            && isset($application['user_group_id']) && $application['user_group_id'] == $fallbackUserGroupId) {
            $this->logger->debug(" - Stanza application $applicationId already assigned to operator $operatorId and group $fallbackUserGroupId");
            return [];
        }

        $data = [
            'user_group_id' => $fallbackUserGroupId,
            'user_id' => $operatorId,
            'message' => '',
            'assigned_at' => date('c'),
        ];

        $this->logger->debug(
            " - Assign stanza application $applicationId to default user and group with payload: " . json_encode($data)
        );
        return $this->doAssign($applicationId, $data);
    }

    /**
     * @param string $applicationId
     * @param array $data
     * @return array|void
     * @throws FailAssignApplication
     */
    private function doAssign(string $applicationId, array $data)
    {
        try {
            $response = (string)$this->client->request(
                'POST',
                $this->apiUri . '/applications/' . $applicationId . '/transition/assign',
                [
                    'headers' => [
                        'Authorization' => 'Bearer ' . $this->getAccessToken(),
                    ],
                    'json' => $data,
                ]
            )->getBody();

            return (array)json_decode($response, true);
        } catch (ClientExceptionInterface $e) {
            CommandException::throwFailAssignApplication($e);
        }
    }

    /**
     * @param array $comment
     * @param array $application
     * @return array
     * @throws FailAssignApplication
     * @throws FailCreateMessage
     */
    public function createMessageFromCommentInApplication(array $comment, array $application): array
    {
        if ((int)$application['status'] <= 2000){
            $this->assignApplicationToDefaultUserAndGroup($application);
        }

        try {
            $data = [
                'message' => $comment['Description__c'],
                'visibility' => 'applicant',
                'sent_at' => $comment['CreatedDate'] ?? date('c'),
                'external_id' => $comment['Id'],
            ];
            $response = (string)$this->client->request(
                'POST',
                $this->apiUri . '/applications/' . $application['id'] . '/messages',
                [
                    'headers' => [
                        'Authorization' => 'Bearer ' . $this->getAccessToken(),
                    ],
                    'json' => $data,
                ]
            )->getBody();
            return json_decode($response, true);
        } catch (ClientExceptionInterface $e) {
            CommandException::throwFailCreateMessage($e);
        }
    }

    /**
     * @param $id
     * @param string $applicationId
     * @return array
     * @throws MessageNotFound
     */
    public function getMessageByExternalId($id, string $applicationId): array
    {
        try {
            $this->logger->debug(' - Get stanza message by external id ' . $id);
            $response = (string)$this->client->request(
                'GET',
                $this->apiUri . '/applications/' . $applicationId . '/messages/byexternal-id/' . $id,
                [
                    'headers' => $this->headers,
                ]
            )->getBody();

            return json_decode($response, true);
        } catch (ClientExceptionInterface $e) {
            CommandException::throwMessageNotFound($e);
        }
    }
}