<?php

namespace Opencontent;

use GuzzleHttp\Exception\ClientException;
use InvalidArgumentException;
use Psr\Http\Client\ClientExceptionInterface;
use Psr\Log\LoggerInterface;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Throwable;

class RunCommand extends Command
{
    /**
     * @var OutputLogger
     */
    protected LoggerInterface $logger;

    protected function configure()
    {
        $this->setName('run');
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $this->logger = new OutputLogger($output, $this->getHelper('formatter'));

        try {

            stream_set_blocking(STDIN, false);
            $inputData = @trim(stream_get_contents(STDIN));
            $data = json_decode($inputData, true);
            if (!$data) {
                $message = json_last_error_msg();
                throw new InvalidArgumentException("Invalid json: $message");
            }

            $this->dispatch($data);

//            $inputSteam = ($input instanceof StreamableInputInterface) ? $input->getStream() : null;
//            $inputSteam = $inputSteam ?? STDIN;
//
//            if ($inputSteam) {
//                $inputData = stream_get_contents($inputSteam);
//                $data = json_decode($inputData, true);
//                if (!$data) {
//                    $message = json_last_error_msg();
//                    throw new InvalidArgumentException("Invalid json: $message");
//                }
//
//                $this->dispatch($data);
//            }

            return self::SUCCESS;
        } catch (Throwable $e) {
            $exception = $e->getPrevious() instanceof ClientException ? $e->getPrevious() : $e;
            $this->logger->error('Error on ' . $exception->getFile() . '#' . $exception->getLine());
            $this->logger->error($e->getMessage());

            if ($exception instanceof ClientException) {
                $errorBody = $exception->getResponse()->getBody();
                $errorJson = json_decode($errorBody);
                if ($errorJson) {
                    $this->logger->debug(json_encode($errorJson));
                } else {
                    $this->logger->debug(substr($errorBody, 0, 2000) . '...');
                }
            }
            if ($output->isVerbose()) {
                $this->logger->debug($exception->getTraceAsString());
            }

            return $e->getCode() > 0 ? $e->getCode() : self::FAILURE;
        }
    }

    /**
     * @param array $event
     * @return void
     * @throws Exceptions\CommandException
     * @throws ClientExceptionInterface
     */
    private function dispatch(array $event): void
    {
        if (!isset($event['entity'])) {
            throw new InvalidArgumentException("Missing entity");
        }

        $stanzaClient = new StanzaClient($this->logger);
        $czRMClient = new CzRMClient($this->logger);

        $entityTypes = (string)getenv('EVENT_ENTITIES');
        $entityTypes = explode(',', $entityTypes);
        $entityTypes = array_map('trim', $entityTypes);

        $entityNormalized = strtolower($event['entity']);
        if (in_array($entityNormalized, $entityTypes)) {
            switch ($entityNormalized) {
                case 'case':
                    (new OnCaseEvent($this->logger, $stanzaClient, $czRMClient))->run($event);
                    break;

                case 'account':
                    (new OnAccountEvent($this->logger, $stanzaClient, $czRMClient))->run($event);
                    break;

                case 'contentversion':
                    (new OnContentVersion($this->logger, $stanzaClient, $czRMClient))->run($event);
                    break;

                case 'commento__c':
                    (new OnCommentEvent($this->logger, $stanzaClient, $czRMClient))->run($event);
                    break;
            }
        }
    }

}