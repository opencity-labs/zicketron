<?php

namespace Opencontent\Exceptions;

use Exception;
use ReflectionClass;
use Throwable;

class CommandException extends Exception
{
    const CASE_NOT_FOUND = 2;

    const ACCOUNT_NOT_FOUND = 3;

    const FAIL_PATCH_CASE = 4;

    const FAIL_PATCH_ACCOUNT = 5;

    const CONTENT_VERSION_NOT_FOUND = 6;

    const CASE_FROM_CONTENT_VERSION_NOT_FOUND = 7;

    const CONTENT_VERSION_DATA_NOT_FOUND = 8;

    const CASE_WITHOUT_ACCOUNT_ID = 9;

    const COMMENT_NOT_FOUND = 10;

    const CASE_FROM_COMMENT_NOT_FOUND = 11;

    const MESSAGE_NOT_FOUND = 12;



    const FAIL_GET_USER_GROUPS = 100;

    const FAIL_CREATE_USER_GROUP = 101;

    const FAIL_APPLICATION_CREATE = 102;

    const APPLICATION_FROM_EXTERNAL_ID_NOT_FOUND = 103;

    const FAIL_BINARY_CREATE = 104;

    const FAIL_USER_CREATE = 105;

    const FAIL_PUT_BINARY_TO_APPLICATION = 106;

    const FAIL_PATCH_APPLICATION_BINARY = 107;

    const FAIL_ACCEPT_APPLICATION = 108;

    const FAIL_ASSIGN_APPLICATION = 109;

    const FAIL_GET_APPLICATION_HISTORY = 110;

    const FAIL_CREATE_MESSAGE = 111;

    const USER_FROM_EXTERNAL_ID_NOT_FOUND = 112;

    const FAIL_GET_APPLICATION_MESSAGES = 113;

    /**
     * @param $message
     * @param $code
     * @param $previous
     * @return never
     */
    protected static function throw($message, $code, $previous = null): never
    {
        $codeString = self::getErrorMessage($code);
        $message = "[Error $code $codeString]  $message";
        throw new static($message, $code, $previous);
    }

    private static function getErrorMessage($value): string
    {
        $class = new ReflectionClass(__CLASS__);
        $constants = array_flip($class->getConstants());

        return $constants[$value] ?? '';
    }

    /**
     * @param Throwable|null $e
     * @return never
     * @throws CaseNotFound
     */
    public static function throwCaseNotFound(?Throwable $e = null): never
    {
        CaseNotFound::throw($e?->getMessage() ?? '', self::CASE_NOT_FOUND, $e);
    }

    /**
     * @param Throwable|null $e
     * @return never
     * @throws CommentNotFound
     */
    public static function throwCommentNotFound(?Throwable $e = null): never
    {
        CommentNotFound::throw($e?->getMessage() ?? '', self::COMMENT_NOT_FOUND, $e);
    }

    /**
     * @param Throwable|null $e
     * @throws AccountNotFound
     */
    public static function throwAccountNotFound(Throwable $e = null): never
    {
        AccountNotFound::throw($e?->getMessage() ?? '', self::ACCOUNT_NOT_FOUND, $e);
    }

    /**
     * @param Throwable|null $e
     * @throws MessageNotFound
     */
    public static function throwMessageNotFound(Throwable $e = null): never
    {
        MessageNotFound::throw($e?->getMessage() ?? '', self::MESSAGE_NOT_FOUND, $e);
    }

    /**
     * @param Throwable|null $e
     * @throws FailPatchCase
     */
    public static function throwFailPatchCase(Throwable $e = null): never
    {
        FailPatchCase::throw($e?->getMessage() ?? '', self::FAIL_PATCH_CASE, $e);
    }

    /**
     * @param Throwable|null $e
     * @throws FailPatchAccount
     */
    public static function throwFailPatchAccount(Throwable $e = null): never
    {
        FailPatchAccount::throw($e?->getMessage() ?? '', self::FAIL_PATCH_ACCOUNT, $e);
    }

    /**
     * @param Throwable|null $e
     * @throws FailApplicationCreate
     */
    public static function throwFailApplicationCreate(Throwable $e = null): never
    {
        FailApplicationCreate::throw($e?->getMessage() ?? '', self::FAIL_APPLICATION_CREATE, $e);
    }

    /**
     * @param Throwable|null $e
     * @throws ContentVersionNotFound
     */
    public static function throwContentVersionNotFound(Throwable $e = null): never
    {
        ContentVersionNotFound::throw($e?->getMessage() ?? '', self::CONTENT_VERSION_NOT_FOUND, $e);
    }

    /**
     * @param Throwable|null $e
     * @throws ContentVersionDataNotFound
     */
    public static function throwContentVersionDataNotFound(Throwable $e = null): never
    {
        ContentVersionDataNotFound::throw($e?->getMessage() ?? '', self::CONTENT_VERSION_DATA_NOT_FOUND, $e);
    }

    /**
     * @param Throwable|null $e
     * @throws CaseFromContentVersionNotFound
     */
    public static function throwCaseFromContentVersionNotFound(Throwable $e = null): never
    {
        CaseFromContentVersionNotFound::throw($e?->getMessage() ?? '', self::CASE_FROM_CONTENT_VERSION_NOT_FOUND, $e);
    }

    /**
     * @param Throwable|null $e
     * @throws ApplicationByExternalIdNotFound
     */
    public static function throwApplicationByExternalIdNotFound(Throwable $e = null): never
    {
        ApplicationByExternalIdNotFound::throw($e?->getMessage() ?? '', self::APPLICATION_FROM_EXTERNAL_ID_NOT_FOUND, $e);
    }

    /**
     * @param Throwable|null $e
     * @throws UserByIdNotFound
     */
    public static function throwUserByIdNotFound(Throwable $e = null): never
    {
        UserByIdNotFound::throw($e?->getMessage() ?? '', self::USER_FROM_EXTERNAL_ID_NOT_FOUND, $e);
    }

    /**
     * @param Throwable|null $e
     * @throws FailBinaryCreate
     */
    public static function throwFailBinaryCreate(Throwable $e = null): never
    {
        FailBinaryCreate::throw($e?->getMessage() ?? '', self::FAIL_BINARY_CREATE, $e);
    }

    /**
     * @param Throwable|null $e
     * @throws FailUserCreate
     */
    public static function throwFailUserCreate(Throwable $e = null): never
    {
        FailUserCreate::throw($e?->getMessage() ?? '', self::FAIL_USER_CREATE, $e);
    }

    /**
     * @param Throwable|null $e
     * @throws CaseWithoutAccountId
     */
    public static function throwCaseWithoutAccountId(Throwable $e = null): never
    {
        CaseWithoutAccountId::throw($e?->getMessage() ?? '', self::CASE_WITHOUT_ACCOUNT_ID, $e);
    }

    /**
     * @param Throwable|null $e
     * @throws FailPutBinaryToApplication
     */
    public static function throwFailPutBinaryToApplication(Throwable $e = null): never
    {
        FailPutBinaryToApplication::throw($e?->getMessage() ?? '', self::FAIL_PUT_BINARY_TO_APPLICATION, $e);
    }

    /**
     * @param Throwable|null $e
     * @throws FailPatchApplicationBinaryWithExternalId
     */
    public static function throwFailPatchApplicationBinaryWithExternalId(Throwable $e = null): never
    {
        FailPatchApplicationBinaryWithExternalId::throw($e?->getMessage() ?? '', self::FAIL_PATCH_APPLICATION_BINARY, $e);
    }

    /**
     * @param Throwable|null $e
     * @throws FailAcceptApplication
     */
    public static function throwFailAcceptApplication(Throwable $e = null): never
    {
        FailAcceptApplication::throw($e?->getMessage() ?? '', self::FAIL_ACCEPT_APPLICATION, $e);
    }

    /**
     * @param Throwable|null $e
     * @throws FailGetUserGroups
     */
    public static function throwFailGetUserGroups(Throwable $e = null): never
    {
        FailGetUserGroups::throw($e?->getMessage() ?? '', self::FAIL_GET_USER_GROUPS, $e);
    }

    /**
     * @param Throwable|null $e
     * @throws FailCreateUserGroup
     */
    public static function throwCreateUserGroup(Throwable $e = null): never
    {
        FailCreateUserGroup::throw($e?->getMessage() ?? '', self::FAIL_CREATE_USER_GROUP, $e);
    }

    /**
     * @param Throwable|null $e
     * @throws FailAssignApplication
     */
    public static function throwFailAssignApplication(Throwable $e = null): never
    {
        FailAssignApplication::throw($e?->getMessage() ?? '', self::FAIL_ASSIGN_APPLICATION, $e);
    }

    /**
     * @param Throwable|null $e
     * @throws FailApplicationHistory
     */
    public static function throwFailApplicationHistory(Throwable $e = null): never
    {
        FailApplicationHistory::throw($e?->getMessage() ?? '', self::FAIL_GET_APPLICATION_HISTORY, $e);
    }

    /**
     * @param Throwable|null $e
     * @throws FailApplicationMessages
     */
    public static function throwFailApplicationMessages(Throwable $e = null): never
    {
        FailApplicationHistory::throw($e?->getMessage() ?? '', self::FAIL_GET_APPLICATION_MESSAGES, $e);
    }

    /**
     * @param Throwable|null $e
     * @throws CaseFromCommentNotFound
     */
    public static function throwCaseFromCommentNotFound(Throwable $e = null): never
    {
        CaseFromCommentNotFound::throw($e?->getMessage() ?? '', self::CASE_FROM_COMMENT_NOT_FOUND, $e);
    }

    /**
     * @param Throwable|null $e
     * @throws FailCreateMessage
     */
    public static function throwFailCreateMessage(Throwable $e = null): never
    {
        FailCreateMessage::throw($e?->getMessage() ?? '', self::FAIL_CREATE_MESSAGE, $e);
    }
}