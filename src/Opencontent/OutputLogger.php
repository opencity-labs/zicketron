<?php

namespace Opencontent;

use Psr\Log\AbstractLogger;
use Psr\Log\LogLevel;
use Stringable;
use Symfony\Component\Console\Helper\FormatterHelper;

//use Symfony\Component\Console\Output\ConsoleOutputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class OutputLogger extends AbstractLogger
{
    public function __construct(
        private readonly OutputInterface $output,
        private readonly FormatterHelper $formatterHelper
    ) {
    }

    public function log($level, Stringable|string $message, array $context = []): void
    {
        if (in_array($level, [
            LogLevel::EMERGENCY,
            LogLevel::ALERT,
            LogLevel::CRITICAL,
            LogLevel::ERROR,
        ])) {
            $this->output->writeln(
                $this->formatterHelper->formatBlock($message, 'error')
            );
//            if ($this->output instanceof ConsoleOutputInterface){
//                $this->output->getErrorOutput()->writeln($message);
//            }
        } elseif (in_array($level, [
            LogLevel::INFO,
            LogLevel::NOTICE,
            LogLevel::WARNING,
        ])) {
            $this->output->writeln(
                $this->formatterHelper->formatBlock($message, 'info')
            );
        } else {
            if ($this->output->isVerbose() || getenv('VERBOSE')) {
                $this->output->writeln($message);
            }
        }
    }
}