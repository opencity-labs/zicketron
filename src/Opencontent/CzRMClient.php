<?php

namespace Opencontent;

use GuzzleHttp\Client;
use Opencontent\Exceptions\AccountNotFound;
use Opencontent\Exceptions\CaseNotFound;
use Opencontent\Exceptions\CommandException;
use Opencontent\Exceptions\CommentNotFound;
use Opencontent\Exceptions\ContentVersionDataNotFound;
use Opencontent\Exceptions\ContentVersionNotFound;
use Opencontent\Exceptions\FailPatchAccount;
use Opencontent\Exceptions\FailPatchCase;
use Psr\Http\Client\ClientExceptionInterface;
use Psr\Log\LoggerInterface;

class CzRMClient
{
    protected Client $client;

    protected static ?string $token = null;

    protected string $apiUri;

    /**
     * @var string[][]
     */
    protected array $headers;

    /**
     * @param LoggerInterface $logger
     * @throws ClientExceptionInterface
     */
    public function __construct(protected readonly LoggerInterface $logger)
    {
        $this->client = new Client();
        $this->apiUri = getenv('CZRM_BASENAME') . '/services/data/v56.0/sobjects';
        $this->headers = [
            'Authorization' => 'Bearer ' . $this->getAccessToken(),
        ];
    }

    /**
     * @param string $id
     * @return array
     * @throws CaseNotFound
     */
    public function getCaseById(string $id): array
    {
        $this->logger->debug(' - Get czrm case by id ' . $id);
        try {
            $response = (string)$this->client->request(
                'GET',
                $this->apiUri . '/Case/' . $id,
                ['headers' => $this->headers]
            )->getBody();

            return json_decode($response, true);
        } catch (ClientExceptionInterface $e) {
            CommandException::throwCaseNotFound($e);
        }
    }

    /**
     * @param string $id
     * @return array
     * @throws CommentNotFound
     */
    public function getCommentById(string $id): array
    {
        $this->logger->debug(' - Get czrm comment by id ' . $id);
        try {
            $response = (string)$this->client->request(
                'GET',
                $this->apiUri . '/Commento__c/' . $id,
                ['headers' => $this->headers]
            )->getBody();

            return json_decode($response, true);
        } catch (ClientExceptionInterface $e) {
            CommandException::throwCommentNotFound($e);
        }
    }

    /**
     * @param string $id
     * @param string $segnalaCiId
     * @return string[]
     * @throws FailPatchCase
     */
    public function patchCaseWithSegnalaCiId(string $id, string $segnalaCiId): array
    {
        $this->logger->debug(' - Patch czrm case id ' . $id . ' with ' . $segnalaCiId);
        try {
            (string)$this->client->request(
                'PATCH',
                $this->apiUri . '/Case/' . $id,
                [
                    'headers' => array_merge($this->headers, ['Content-Type' => 'application/json',]),
                    'json' => [
                        'SegnalaCiId__c' => $segnalaCiId,
                    ],
                ]
            )->getBody();

            return [
                'Id' => $id,
                'SegnalaCiId__c' => $segnalaCiId,
                'SegnalaCiId__pc' => $segnalaCiId,
            ];
        } catch (ClientExceptionInterface $e) {
            CommandException::throwFailPatchCase($e);
        }
    }

    /**
     * @param $id
     * @return array
     * @throws AccountNotFound
     */
    public function getAccountById($id): array
    {
        $this->logger->debug(' - Get czrm account by id ' . $id);
        try {
            $response = (string)$this->client->request(
                'GET',
                $this->apiUri . '/Account/' . $id,
                ['headers' => $this->headers]
            )->getBody();

            return json_decode($response, true);
        } catch (ClientExceptionInterface $e) {
            CommandException::throwAccountNotFound($e);
        }
    }

    /**
     * @param $id
     * @return array
     * @throws ContentVersionNotFound
     */
    public function getContentVersionById($id): array
    {
        try {
            $this->logger->debug(' - Get czrm content version by id ' . $id);
            $response = (string)$this->client->request(
                'GET',
                $this->apiUri . '/ContentVersion/' . $id,
                ['headers' => $this->headers]
            )->getBody();

            return json_decode($response, true);
        } catch (ClientExceptionInterface $e) {
            CommandException::throwContentVersionNotFound($e);
        }
    }

    /**
     * @param $id
     * @return string
     * @throws ContentVersionDataNotFound
     */
    public function getContentVersionByData($id): string
    {
        try {
            $this->logger->debug(' - Get czrm content version data by id ' . $id);
            $response = $this->client->request(
                'GET',
                $this->apiUri . '/ContentVersion/' . $id . '/VersionData',
                ['headers' => $this->headers]
            )->getBody();

            return $response->getContents();
        } catch (ClientExceptionInterface $e) {
            CommandException::throwContentVersionDataNotFound($e);
        }
    }

    /**
     * @param string $id
     * @param string $segnalaCiId
     * @return string[]
     * @throws FailPatchAccount
     */
    public function patchAccountWithSegnalaCiId(string $id, string $segnalaCiId): array
    {
        $this->logger->debug(' - Patch czrm account id ' . $id . ' with ' . $segnalaCiId);
        try {
            (string)$this->client->request(
                'PATCH',
                $this->apiUri . '/Account/' . $id,
                [
                    'headers' => array_merge($this->headers, ['Content-Type' => 'application/json',]),
                    'json' => [
                        'SegnalaCiId__c' => $segnalaCiId,
//                        'SegnalaCiId__pc' => $segnalaCiId,
                    ],
                ]
            )->getBody();

            return [
                'Id' => $id,
                'SegnalaCiId__c' => $segnalaCiId,
            ];
        } catch (ClientExceptionInterface $e) {
            CommandException::throwFailPatchAccount($e);
        }
    }

    /**
     * @return string
     * @throws ClientExceptionInterface
     */
    public function getAccessToken(): string
    {
        if (self::$token === null) {
//            $this->logger->debug(' - Get czrm access token');

            $authType = getenv('CZRM_AUTH_TYPE');
            if (strtoupper($authType) === 'WSO2') {
                $requestData = [
                    'json' => [
                        'Key' => getenv('CZRM_AUTH_TOKEN_CLIENT_ID'),
                        'Secret' => getenv('CZRM_AUTH_TOKEN_CLIENT_SECRET'),
                    ],
                ];
            } else {
                $requestData = [
                    'multipart' => [
                        [
                            'name' => 'username',
                            'contents' => getenv('CZRM_AUTH_TOKEN_USER'),
                        ],
                        [
                            'name' => 'password',
                            'contents' => getenv('CZRM_AUTH_TOKEN_PASSWORD'),
                        ],
                        [
                            'name' => 'grant_type',
                            'contents' => getenv('CZRM_AUTH_TOKEN_GRANT_TYPE'),
                        ],
                        [
                            'name' => 'client_id',
                            'contents' => getenv('CZRM_AUTH_TOKEN_CLIENT_ID'),
                        ],
                        [
                            'name' => 'client_secret',
                            'contents' => getenv('CZRM_AUTH_TOKEN_CLIENT_SECRET'),
                        ],
                    ],
                ];
            }

            $response = json_decode(
                (string)$this->client->request(
                    'POST',
                    getenv('CZRM_AUTH_TOKEN_ENDPOINT'),
                    $requestData
                )->getBody(),
                true
            );
            self::$token = $response['access_token'];
        }

        return self::$token;
    }

    /**
     * @param string $externalId
     * @param array $attachment
     * @param string $base64Data
     * @return array
     * @throws ClientExceptionInterface
     */
    public function postCaseContentVersion(string $externalId, array $attachment, string $base64Data): array
    {
        $this->logger->debug(' - Post czrm ContentVersion of case id ' . $externalId . ' with name ' . $attachment['originalName']);
        $data = [
            'Title' => $attachment['originalName'] ?? null,
            'PathOnClient' => $attachment['originalName'] ?? null,
            'firstPublishLocationid' => $externalId,
            'VersionData' => $base64Data,
        ];
        $response = (string)$this->client->request(
            'POST',
            $this->apiUri . '/ContentVersion',
            [
                'headers' => array_merge($this->headers, ['Content-Type' => 'application/json',]),
                'json' => $data,
            ]
        )->getBody();

        return json_decode($response, true);
    }
}
