<?php

namespace Opencontent;

use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class DebugCommand extends Command
{
    private static $debugCommands = [
        'dump-env',
        'czrm-token',
        'sdc-token',
    ];

    protected function configure()
    {
        $this->setName('debug')->addArgument(
            'debug-name',
            InputArgument::OPTIONAL,
            implode(', ', self::$debugCommands)
        );
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $logger = new OutputLogger($output, $this->getHelper('formatter'));
        if ($input->hasArgument('debug-name')) {
            $debug = $input->getArgument('debug-name');

            if ($debug === 'dump-env') {
                $env = getenv();
                ksort($env);
                print_r($env);
            } elseif ($debug === 'czrm-token') {
                $output->writeln((new CzRMClient($logger))->getAccessToken());
            } elseif ($debug === 'sdc-token') {
                $output->writeln((new StanzaClient($logger))->getAccessToken());
            } else {
                $output->writeln('Argument not found: select one of ' . implode(', ', self::$debugCommands));
            }
        }

        return self::SUCCESS;
    }
}