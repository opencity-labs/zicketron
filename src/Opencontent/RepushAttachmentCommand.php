<?php

namespace Opencontent;

use Psr\Http\Client\ClientExceptionInterface;
use Psr\Log\LoggerInterface;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;

class RepushAttachmentCommand extends Command
{
    /**
     * @var OutputLogger
     */
    protected LoggerInterface $logger;

    protected ?StanzaClient $stanzaClient;

    protected ?CzRMClient $czRMClient;

    protected function configure()
    {
        $this->setName('repush_application_attachments')
            ->addOption('application-id', null, InputOption::VALUE_REQUIRED, 'comma separated application id list');
    }

    public function execute(InputInterface $input, OutputInterface $output): int
    {
        $applicationIdList = $input->hasOption('application-id') ? $input->getOption('application-id') : null;
        $this->logger = new OutputLogger($output, $this->getHelper('formatter'));
        $applicationIdList = explode(',', $applicationIdList);

        $this->stanzaClient = new StanzaClient($this->logger);
        $this->czRMClient = new CzRMClient($this->logger);

        try {
            foreach ($applicationIdList as $applicationId) {
                $this->repush($applicationId);
            }

            return self::SUCCESS;
        } catch (\Throwable $e) {
            $this->logger->error($e->getMessage());
            $this->logger->debug($e->getTraceAsString());
            return self::FAILURE;
        }
    }

    /**
     * @param $applicationId
     * @return bool
     * @throws Exceptions\FailPatchApplicationBinaryWithExternalId
     * @throws ClientExceptionInterface
     */
    private function repush($applicationId): bool
    {
        $applicationId = trim($applicationId);
        if (empty($applicationId)) {
            return false;
        }
        $this->logger->info('Working on application ' . $applicationId);

        $application = $this->stanzaClient->getApplicationByUuid($applicationId);
        $applicationExternalId = $application['external_id'] ?? null;
        if (!$applicationExternalId) {
            throw new \InvalidArgumentException('> Application does not have an external_id');
        }

        foreach ($application['attachments'] as $attachment) {
            $externalId = $attachment['external_id'] ?? null;
            if (!$externalId) {
                $this->logger->info('> Push attachment ' . $attachment['id']);
                $base64Data = base64_encode(
                    $this->stanzaClient->request('GET', $attachment['url'])->getBody()
                );
                $contentVersion = $this->czRMClient->postCaseContentVersion(
                    $applicationExternalId,
                    $attachment,
                    $base64Data
                );
                $contentVersionId = $contentVersion['id'] ?? null;
                if ($contentVersionId) {
                    $this->stanzaClient->patchApplicationBinaryWithExternalId(
                        $applicationId,
                        $attachment['id'],
                        $contentVersionId
                    );
                }
            } else {
                $this->logger->info('> Attachment ' . $attachment['id'] . ' already pushed');
            }
        }

        return true;
    }
}