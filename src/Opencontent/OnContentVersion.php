<?php

namespace Opencontent;

use Psr\Http\Client\ClientExceptionInterface;

class OnContentVersion extends AbstractEvent
{
    /**
     * @param array $event
     * @return void
     * @throws ClientExceptionInterface
     * @throws Exceptions\ApplicationByExternalIdNotFound
     * @throws Exceptions\CaseFromContentVersionNotFound
     * @throws Exceptions\ContentVersionDataNotFound
     * @throws Exceptions\ContentVersionNotFound
     * @throws Exceptions\FailBinaryCreate
     * @throws Exceptions\FailPatchApplicationBinaryWithExternalId
     * @throws Exceptions\FailPutBinaryToApplication
     * @throws Exceptions\FailApplicationMessages
     */
    public function run(array $event): void
    {
        $id = $event['id'];
        $this->logger->info("Working on ContentVersion $id");

        $contentVersion = $this->czRmClient->getContentVersionById($id);
        $caseId = $contentVersion['FirstPublishLocationId'] ?? null;
        if (!$caseId) {
            Exceptions\CommandException::throwCaseFromContentVersionNotFound();
        }
        $this->logger->warning(" - Case id is $caseId");
        $application = $this->stanzaClient->getApplicationByExternalId($caseId, 2);
        $alreadyExists = false;
        foreach ($application['attachments'] as $attachment) {
            if ($attachment['external_id'] === $id) {
                $this->logger->debug(" - ContentVersion id $id already exists as attachment external_id");
                $alreadyExists = true;
            }
        }

        // è un allegato di un commento?
        $messages = $this->stanzaClient->getApplicationMessages($application['id']);
        foreach ($messages as $message){
            $attachments = $message['attachments'] ?? [];
            foreach ($attachments as $attachment){
                if ($attachment['external_id'] === $id) {
                    $this->logger->debug(" - ContentVersion id $id already exists as message attachment external_id");
                    $alreadyExists = true;
                }
            }
        }

        if (!$alreadyExists) {
            $fileName = $contentVersion['PathOnClient'];
            $fileExtension = '.' . $contentVersion['FileExtension'];
            if (stripos($fileName, $fileExtension) !== false) {
                $fileExtension = '';
            }
            $filePath = sys_get_temp_dir() . '/' . $fileName . $fileExtension;
            file_put_contents($filePath, $this->czRmClient->getContentVersionByData($id));
            $mimeType = mime_content_type($filePath);

            $data = [
                'original_filename' => $fileName,
                'description' => $contentVersion['Description'],
                'protocol_required' => false,
                'mime_type' => $mimeType,
                'path' => $filePath,
            ];
            $uploadFile = $this->stanzaClient->uploadBinary($data);
            @unlink($filePath);

            $dataKey = stripos($mimeType, 'image') !== false ? 'images' : 'docs';
            $this->stanzaClient->addBinaryToApplication($uploadFile, $application, $dataKey, $caseId, $id);
        }
    }
}