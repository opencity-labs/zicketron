<?php

namespace Opencontent;

use Psr\Log\LoggerInterface;

abstract class AbstractEvent
{
    public function __construct(
        protected readonly LoggerInterface $logger,
        protected readonly StanzaClient $stanzaClient,
        protected readonly CzRMClient $czRmClient
    ) {
    }

    abstract public function run(array $event): void;
}