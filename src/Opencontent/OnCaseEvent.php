<?php

namespace Opencontent;

use Psr\Http\Client\ClientExceptionInterface;

class OnCaseEvent extends AbstractEvent
{
    /*
     * Crea user se necessario in base al case.AccountId->SegnalaCiId__c e a case.AccountId->Codice_Fiscale__c/Codice_Fiscale__pc
     * Crea application se necessario in base a case.SegnalaCiId__c e aggiorna application.external_id in base a case.SegnalaCiId__c
     * Aggiorna user.external_id in base a account.SegnalaCiId__c
     * Crea user group in base a Direzione_Ufficio__c se necessario
     * Aggiorna lo stato di application in base a Stato_Front_End__c
     *
     * Usa l'autenticazione admin per la creazione dell'ufficio
     */

    /**
     * @param array $event
     * @return void
     * @throws Exceptions\AccountNotFound
     * @throws Exceptions\CaseNotFound
     * @throws Exceptions\CaseWithoutAccountId
     * @throws Exceptions\FailCreateUserGroup
     * @throws Exceptions\FailAcceptApplication
     * @throws Exceptions\FailApplicationCreate
     * @throws Exceptions\FailAssignApplication
     * @throws Exceptions\FailGetUserGroups
     * @throws Exceptions\FailPatchAccount
     * @throws Exceptions\FailPatchCase
     * @throws Exceptions\FailUserCreate
     * @throws ClientExceptionInterface
     */
    public function run(array $event): void
    {
        $id = $event['id'];
        $this->logger->info("Working on case $id");

        try {
            $application = $this->stanzaClient->getApplicationByExternalId($id);
        }catch (Exceptions\ApplicationByExternalIdNotFound){
            $application = [];
        }

        $case = $this->czRmClient->getCaseById($id);

        if (empty($case['AccountId'])) {
            Exceptions\CommandException::throwCaseWithoutAccountId();
        }
        $account = $this->czRmClient->getAccountById($case['AccountId']);
        $fiscalCode = Utils::getFirstNotEmpty($account, 'Codice_Fiscale__c', 'Codice_Fiscale__pc');

        $user = null;
        if (empty($account['SegnalaCiId__c']) && !empty($fiscalCode)) {
            $user = $this->stanzaClient->createUserFromAccount($account);
            $accountPatched = $this->czRmClient->patchAccountWithSegnalaCiId($account['Id'], $user['id']);
            $account['SegnalaCiId__c'] = empty($accountPatched['SegnalaCiId__c']) ? null : $accountPatched['SegnalaCiId__c'];
        }elseif ($account['SegnalaCiId__c']){
            try{
                $user = $this->stanzaClient->getUserById($account['SegnalaCiId__c']);
            }catch (Exceptions\UserByIdNotFound $e){
                $this->logger->error($e->getMessage());
            }
        }

        if (empty($application)) {
            $application = $this->stanzaClient->createApplicationFromCase($case, $user);
            if (empty($case['SegnalaCiId__c']) && isset($application['id'])) {
                $this->czRmClient->patchCaseWithSegnalaCiId($id, $application['id']);
            }
        }

        if (empty($account['SegnalaCiId__c'])) {
            $this->czRmClient->patchAccountWithSegnalaCiId($account['Id'], $user['id'] ?? $application['user']);
        }

        if (!empty($application)) {
            $this->syncApplicationStatus($application, $case);
        }
    }

    /**
     * @param array $application
     * @param array $case
     * @return void
     * @throws Exceptions\FailCreateUserGroup
     * @throws Exceptions\FailAcceptApplication
     * @throws Exceptions\FailAssignApplication
     * @throws Exceptions\FailGetUserGroups
     */
    private function syncApplicationStatus(array $application, array $case): void
    {
        $caseStatus = $case['Stato_Front_End__c'] ?? false;
        $caseOfficeName = $case['Direzione_Ufficio__c'] ?? false;

        $userGroup = null;
        if (!empty($caseOfficeName)){
            $userGroup = $this->stanzaClient->getUserGroupByName($caseOfficeName);
        }
        if ($userGroup !== null){
            $this->stanzaClient->assignApplicationToUserGroup($application, $userGroup);
        }
        if ($caseStatus === 'Chiuso') {
            $this->stanzaClient->acceptApplication($application);
        }
    }
}