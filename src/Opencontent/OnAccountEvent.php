<?php

namespace Opencontent;

class OnAccountEvent extends AbstractEvent
{
    /**
     * Crea user se necessario in base al account.SegnalaCiId__c e a account.Codice_Fiscale__c/Codice_Fiscale__pc
     *
     * @param array $event
     * @return void
     * @throws Exceptions\AccountNotFound
     * @throws Exceptions\FailPatchAccount
     * @throws Exceptions\FailUserCreate
     */
    public function run(array $event): void
    {
        $id = $event['id'];
        $this->logger->info("Working on account $id");

        $account = $this->czRmClient->getAccountById($id);
        $fiscalCode = Utils::getFirstNotEmpty($account, 'Codice_Fiscale__c', 'Codice_Fiscale__pc');
        if (empty($account['SegnalaCiId__c']) && !empty($fiscalCode)) {
            $user = $this->stanzaClient->createUserFromAccount($account);
            $this->czRmClient->patchAccountWithSegnalaCiId($account['Id'], $user['id']);
        }
    }
}