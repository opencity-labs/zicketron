<?php

namespace Opencontent;

use Opencontent\Exceptions\CommandException;
use Opencontent\Exceptions\MessageNotFound;
use Psr\Http\Client\ClientExceptionInterface;

class OnCommentEvent extends AbstractEvent
{

    /**
     * @param array $event
     * @return void
     * @throws ClientExceptionInterface
     * @throws Exceptions\ApplicationByExternalIdNotFound
     * @throws Exceptions\CaseFromCommentNotFound
     * @throws Exceptions\CommentNotFound
     * @throws Exceptions\FailApplicationHistory
     * @throws Exceptions\FailAssignApplication
     * @throws Exceptions\FailCreateMessage
     */
    public function run(array $event): void
    {
        $id = $event['id'];
        $this->logger->info("Working on comment $id");

        $comment = $this->czRmClient->getCommentById($id);
        $caseId = $comment['CaseId__c'] ?? null;
        if (!$caseId) {
            CommandException::throwCaseFromCommentNotFound();
        }

        $this->logger->warning(" - Case id is $caseId");

        $application = $this->stanzaClient->getApplicationByExternalId($caseId);

        if ($comment['Tipo_Commento__c'] === 'Pubblicato') {
            try {
                $this->stanzaClient->getMessageByExternalId($id, $application['id']);
            }catch (MessageNotFound) {
                $this->stanzaClient->createMessageFromCommentInApplication($comment, $application);
            }
        }
    }

}